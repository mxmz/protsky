module example.com/provaproxy

go 1.20

require (
	github.com/elazarl/goproxy v0.0.0-20231117061959-7cc037d33fb5
	github.com/samber/lo v1.39.0
)

require golang.org/x/exp v0.0.0-20220303212507-bbda1eaf7a17 // indirect
