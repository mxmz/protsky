package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"flag"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"regexp"

	"github.com/elazarl/goproxy"
	"github.com/samber/lo"
)

type ConfigEntry struct {
	Regexp string `json:"regexp,omitempty"`
	Proxy  string `json:"proxy,omitempty"`
}

func main() {
	verbose := flag.Bool("v", false, "should every proxy request be logged to stdout")
	configFile := flag.String("config", "./proxies.json", "Proxy list")
	addr := flag.String("addr", ":8080", "proxy listen address")
	flag.Parse()
	proxy := goproxy.NewProxyHttpServer()
	proxy.Verbose = *verbose

	var config []ConfigEntry
	err := json.Unmarshal(OrPanic(os.ReadFile(*configFile)), &config)
	if err != nil {
		panic(err)
	}
	proxy.ConnectDialWithReq = NewConnectDialToProxy(config)

	log.Fatal(http.ListenAndServe(*addr, proxy))
}

func OrPanic[T any](v T, err error) T {
	if err != nil {
		panic(err)
	}
	return v
}

func NewConnectDialToProxy(config []ConfigEntry) func(req *http.Request, network, addr string) (net.Conn, error) {
	type parsedConfigEntry struct {
		Regexp *regexp.Regexp
		Proxy  *url.URL
	}

	routes := lo.Map(config, func(e ConfigEntry, _ int) parsedConfigEntry {
		return parsedConfigEntry{Regexp: regexp.MustCompile(e.Regexp), Proxy: OrPanic(url.Parse(e.Proxy))}
	})

	return func(req *http.Request, network, addr string) (net.Conn, error) {

		var u *url.URL

		for _, v := range routes {
			if v.Regexp.Match([]byte(req.Host)) {
				u = v.Proxy
				break
			}
		}
		if u.Host == "" {
			c, err := net.Dial(network, addr)
			return c, err
		} else {
			connectReq := &http.Request{
				Method: "CONNECT",
				URL:    &url.URL{Opaque: addr},
				Host:   addr,
				Header: make(http.Header),
			}

			c, err := net.Dial(network, u.Host)
			if err != nil {
				return nil, err
			}
			connectReq.Write(c)
			// Read response.
			// Okay to use and discard buffered reader here, because
			// TLS server will not speak until spoken to.
			br := bufio.NewReader(c)
			resp, err := http.ReadResponse(br, connectReq)
			if err != nil {
				c.Close()
				return nil, err
			}
			defer resp.Body.Close()
			if resp.StatusCode != 200 {
				resp, err := ioutil.ReadAll(resp.Body)
				if err != nil {
					return nil, err
				}
				c.Close()
				return nil, errors.New("proxy refused connection" + string(resp))
			}
			return c, nil
		}
	}

}
